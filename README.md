# TAU Reverse Workshop 

Welcome to Reverse Engineering 101!


Try to reverse each challenge and discover which password is needed!

```
password00 - easy
password01 - easy
password02 - medium
password03 - medium
rock_paper_scissors - hard
```

For rock_paper_scissors all you need is too win, no  password is needed.

 by NVIDIA security team
 © NVIDIA


